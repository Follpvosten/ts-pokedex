import * as TelegramBot from 'node-telegram-bot-api';
import * as fs from 'fs';
import { UserEntry } from './interfaces';
import { gens, maxId } from './data';
import { getName, findPokemon, checkRange, pageLength, createList } from './util';

console.log("Loading user data...");
var userData: UserEntry[] = [];
if (fs.existsSync("userData.json"))
    userData = JSON.parse(fs.readFileSync("userData.json", "utf-8"));

const getUserEntry = (userId: number) => {
    let userEntry = userData.find(ud => ud.id == userId);
    if (!userEntry) {
        userEntry = { id: userId, caught: [] };
        userData.push(userEntry);
    }
    return userEntry;
};

// Check for token file
if (!fs.existsSync("token.txt")) {
    console.error("FATAL: No bot token available!\n"
        + "Please provide one in `token.txt`.");
    fs.writeFileSync("token.txt", "");
    process.exit(1);
}
// Load token and check content
const token = fs.readFileSync("token.txt", "utf-8").trim();
if (!token) {
    console.error("FATAL: No bot token available!\n"
        + "Please provide one in `token.txt`.");
    process.exit(1);
}

const bot = new TelegramBot(token, { polling: true });

bot.onText(/^\/catch (.+)/i, (msg, match) => {
    let sendOptions: TelegramBot.SendMessageOptions = {
        reply_to_message_id: msg.message_id,
        disable_web_page_preview: true
    };
    let userEntry = getUserEntry(msg.from.id);
    let foundNames: string[] = [];
    let errorNames: string[] = [];
    for (let searchStr of match[1].split(" ")) {
        try {
            let foundPkmn = findPokemon(searchStr);
            if (userEntry.caught.includes(foundPkmn.id)) {
                errorNames.push(`${getName(foundPkmn.id, msg.from.language_code)} (already caught)`);
            } else {
                userEntry.caught.push(foundPkmn.id);
                foundNames.push(getName(foundPkmn.id, msg.from.language_code));
            }
        } catch (e) {
            errorNames.push(`${searchStr} (not found)`);
        }
    }
    if (foundNames.length == 0) foundNames.push("-");
    if (errorNames.length == 0) errorNames.push("-");

    if (userEntry.listMessage && foundNames[0] != "-") {
        bot.editMessageText(
            createList(userEntry, msg.from.language_code),
            {
                message_id: userEntry.listMessage.id,
                chat_id: userEntry.listMessage.chat_id,
                reply_markup: { inline_keyboard: navKeyboard }
            }
        );
    }
    fs.writeFile("userData.json", JSON.stringify(userData), () => {
        let result =
            `Successfully registered: ${foundNames.join(", ")}
There were problems with: ${errorNames.join(", ")}`;
        bot.sendMessage(msg.chat.id, result, sendOptions);
    });
});
bot.onText(/^\/release (.+)/i, (msg, match) => {
    let sendOptions: TelegramBot.SendMessageOptions = {
        reply_to_message_id: msg.message_id,
        disable_web_page_preview: true
    };
    let userEntry = getUserEntry(msg.from.id);
    let foundNames: string[] = [];
    let errorNames: string[] = [];
    for (let searchStr of match[1].split(" ")) {
        try {
            let foundPkmn = findPokemon(searchStr);
            if (!userEntry.caught.includes(foundPkmn.id)) {
                errorNames.push(`${getName(foundPkmn.id, msg.from.language_code)} (not caught)`);
            } else {
                userEntry.caught.splice(userEntry.caught.indexOf(foundPkmn.id), 1);
                foundNames.push(getName(foundPkmn.id, msg.from.language_code));
            }
        } catch (e) {
            errorNames.push(`${searchStr} (not found)`);
        }
    }
    if (foundNames.length == 0) foundNames.push("-");
    if (errorNames.length == 0) errorNames.push("-");

    if (userEntry.listMessage && foundNames[0] != "-") {
        bot.editMessageText(
            createList(userEntry, msg.from.language_code),
            {
                message_id: userEntry.listMessage.id,
                chat_id: userEntry.listMessage.chat_id,
                reply_markup: { inline_keyboard: navKeyboard }
            }
        );
    }
    fs.writeFile("userData.json", JSON.stringify(userData), () => {
        let result =
            `Successfully released: ${foundNames.join(", ")}
There were problems with: ${errorNames.join(", ")}`;
        bot.sendMessage(msg.chat.id, result, sendOptions);
    });
});

bot.onText(/^\/gen([1-7]|all)$/i, (msg, match) => {
    let sendOptions = { reply_to_message_id: msg.message_id };
    let userEntry = getUserEntry(msg.from.id);
    if (match[1] == "all") {
        bot.sendMessage(msg.chat.id, checkRange(1, maxId, userEntry), sendOptions);
    } else {
        let gen = gens[Number(match[1]) - 1];
        bot.sendMessage(msg.chat.id, checkRange(gen.start, gen.end, userEntry), sendOptions);
    }
});

bot.onText(/^\/check (.+)/i, (msg, match) => {
    let sendOptions: TelegramBot.SendMessageOptions = {
        reply_to_message_id: msg.message_id,
        disable_web_page_preview: true
    };
    let userEntry = getUserEntry(msg.from.id);
    if (/[0-9] *- *[0-9]/.test(match[1])) {
        let bounds = match[1].split("-").map(n => Number(n));
        bot.sendMessage(msg.chat.id, checkRange(bounds[0], bounds[1], userEntry), sendOptions);
    } else {
        try {
            let foundPkmn = findPokemon(match[1], msg.from.language_code);
            let result = `Pokemon #${foundPkmn.id} (${getName(foundPkmn.id, msg.from.language_code)})\n
Current status: ${userEntry.caught.includes(foundPkmn.id) ? "Found" : "Not found"}`;
            bot.sendMessage(msg.chat.id, result, sendOptions);
        } catch (e) {
            bot.sendMessage(msg.chat.id, e, sendOptions);
        }
    }
});

const navKeyboard = [
    [{ text: "\u{2B05}", callback_data: "prev" }, { text: "\u{27A1}", callback_data: "next" }]
];
bot.onText(/^\/list$/i, (msg) => {
    const sendOptions: TelegramBot.SendMessageOptions = {
        reply_markup: {
            inline_keyboard: navKeyboard
        }
    };
    let userEntry = getUserEntry(msg.from.id);
    if (!userEntry.currentPage) userEntry.currentPage = 0;
    bot.sendMessage(msg.chat.id, createList(userEntry, msg.from.language_code), sendOptions)
        .then((message: TelegramBot.Message) => {
            userEntry.listMessage = { id: message.message_id, chat_id: message.chat.id };
            fs.writeFile("userData.json", JSON.stringify(userData), () => { });
        });
});
bot.on('callback_query', (query: TelegramBot.CallbackQuery) => {
    if (!query.message) return;
    let userEntry = getUserEntry(query.from.id);
    if (!userEntry.listMessage || userEntry.listMessage.id != query.message.message_id) {
        bot.answerCallbackQuery(query.id, {
            text: "Error: Not your (current) dex, run /list"
        });
        return;
    }
    if (query.data == "prev") {
        if (userEntry.currentPage == 0) {
            bot.answerCallbackQuery(query.id, { text: "Showing the first page." });
            return;
        }
        userEntry.currentPage--;
    } else if (query.data == "next") {
        if ((userEntry.currentPage) * pageLength + 1 > maxId) {
            bot.answerCallbackQuery(query.id, { text: "Showing the last page." });
            return;
        }
        userEntry.currentPage++;
    } else return; // Unknown action; cancel.
    bot.editMessageText(
        createList(userEntry, query.from.language_code),
        {
            message_id: userEntry.listMessage.id,
            chat_id: userEntry.listMessage.chat_id,
            reply_markup: { inline_keyboard: navKeyboard }
        })
        .then(() => bot.answerCallbackQuery(query.id, {
            text: "Showing page " + (userEntry.currentPage + 1)
        }));
});

bot.onText(/^(\/help|\/start)$/i, msg => {
    let sendOptions = { reply_to_message_id: msg.message_id };
    getUserEntry(msg.from.id);
    const result = `Pokedex Bot v0.0.2

Available commands:
/start or /help - Display this message
(A "pkmn" can be either a Pokemon name in english, german or french, or a national Pokedex number)
/catch <pkmn1> [pkmn2] ... [pkmnN] - Declares a Pokemon as caught
/release <pkmn1> [pkmn2] ... [pkmnN] - Revokes a "catch"
/check <pkmn|range> - Either checks if you have caught a specific Pokemon, or reports your progress in a specific range (e.g. 100-150)
/gen<1-7> - Reports your progress in the specified generation of Pokemon
/list - Displays an interactive Pokedex-style Pokemon list that is refreshed whenever you catch or release a Pokemon
/export - Outputs your data (your caught Pokemon) as an easily machine-readable list`;
    bot.sendMessage(msg.chat.id, result, sendOptions);
});

bot.onText(/^\/export$/i, msg => {
    let sendOptions = { reply_to_message_id: msg.message_id };
    let userEntry = getUserEntry(msg.from.id);
    if (userEntry.caught.length < 1) {
        bot.sendMessage(msg.chat.id, "You don't have any data to export!", sendOptions);
        return;
    }
    if (msg.chat.type == 'group' || msg.chat.type == 'supergroup') {
        bot.sendMessage(msg.from.id, JSON.stringify(userEntry.caught))
            .then(() => {
                bot.sendMessage(msg.from.id, "You will be able to forward this message to an /import command in the future.");
                bot.sendMessage(msg.chat.id, "I have sent you a private message with your data.", sendOptions);
            }).catch(() => {
                bot.sendMessage(msg.chat.id, "Please send me a private message so I can send you your data.", sendOptions);
            });
    } else if (msg.chat.type == 'private') {
        bot.sendMessage(msg.chat.id, JSON.stringify(userEntry.caught)).then(() => {
            bot.sendMessage(msg.chat.id, "You will be able to forward this message to an /import command in the future.");
        });
    } else {
        // Unsupported.
        console.error("Got unsupported chat type: " + msg.chat.type);
    }
});

console.log("Init complete!");
