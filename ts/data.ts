import { pkmnz } from './pokemon';

export const maxId = pkmnz[pkmnz.length - 1].id;

console.log("Indexing names...");
export interface PkName { name: string, id: number }
export const nameIndex = ([] as PkName[]).concat(...pkmnz.map(pkmn => {
    return <PkName[]>[
        { name: pkmn.name_en.toLowerCase(), id: pkmn.id },
        { name: pkmn.name_de.toLowerCase(), id: pkmn.id },
        { name: pkmn.name_fr.toLowerCase(), id: pkmn.id }
    ];
}));

export interface Gen { start: number, end: number }
export const gens: Gen[] = [
    { start: 1, end: 151 },
    { start: 152, end: 251 },
    { start: 252, end: 386 },
    { start: 387, end: 493 },
    { start: 494, end: 649 },
    { start: 650, end: 721 },
    { start: 722, end: maxId }
];
