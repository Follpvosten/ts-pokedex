
export interface UserEntry {
    id: number,
    caught: number[],
    listMessage?: {
        id: number,
        chat_id: number
    },
    currentPage?: number
}
