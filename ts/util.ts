import { pkmnz } from './pokemon';
import { UserEntry } from './interfaces';
import { nameIndex, maxId } from './data';

export const getName = (pkmnId: number, userLang: string) => {
    let pkmn = pkmnz.find(pkmn => pkmn.id == pkmnId);
    if (!userLang) return pkmn.name_en;
    if (userLang.startsWith("en"))
        return pkmn.name_en;
    if (userLang.startsWith("de"))
        return pkmn.name_de;
    if (userLang.startsWith("fr"))
        return pkmn.name_fr;
};

export const findPokemon = (match: string, userLang?: string) => {
    let pkmnId = Number(match);
    if (isNaN(pkmnId)) {
        // Search string
        let foundPkName = nameIndex.find(name => name.name == match.toLowerCase());
        if (foundPkName)
            return pkmnz.find(pkmn => pkmn.id == foundPkName.id);
        else {
            if (!userLang) throw "";
            let results = nameIndex
                .filter(pkName => {
                    return pkName.name.toLowerCase().includes(match.toLowerCase());
                }).map(pkName => getName(pkName.id, userLang));
            if (results.length > 0) {
                throw `No Pokemon found for "${match}"!\n
Did you mean any of these: ${Array.from(new Set(results)).join(", ")}?`;
            } else
                throw `No Pokemon found for ${match}!`;
        }
    } else {
        // ID
        if (pkmnId > maxId || pkmnId < 1)
            throw `Error: Please enter a number between 1 and ${maxId}!`;
        return pkmnz.find(pkmn => pkmn.id == pkmnId);
    }
};

export const checkRange = (min: number, max: number, userEntry: UserEntry) => {
    if (min > max) {
        let tmp = min;
        min = max;
        max = tmp;
    }
    let pkmnList = pkmnz.filter(p => p.id >= min && p.id <= max);
    if (pkmnList.length < 1) {
        return "No Pokemon found!";
    } else {
        let numCaught = pkmnList.filter(p => userEntry.caught.includes(p.id)).length;
        let percent = ((numCaught / pkmnList.length) * 100).toFixed(2);
        return `In ${min}-${max}: ${numCaught}/${pkmnList.length} caught (${percent}%)`;
    }
};

export const pageLength = 25;
export const createList = (userEntry: UserEntry, userLang?: string) => {
    let startId = userEntry.currentPage * pageLength + 1;
    let endId = startId + pageLength;
    if (endId > maxId) endId = maxId + 1;
    let headline = `${startId} to ${endId - 1} (page ${userEntry.currentPage + 1}):\n\n`;
    let rows: string[] = [];
    for (let i = startId; i < endId; i++) {
        let text = `#${i}: ${getName(i, userLang)}`
        if (userEntry.caught.includes(i))
            text += " \u{2705}";
        rows.push(text);
    }
    return headline + rows.join("\n");
};
